package wf

import "fmt"

type UnknownWorkflow struct {
	wfType string
}

func (err UnknownWorkflow) Error() string {
	return fmt.Sprintf("Unknown workflow type %s", err.wfType)
}

type UnknownWorkflowAction struct {
}

func (err UnknownWorkflowAction) Error() string {
	return "Unknown workflow action"
}

type WorkflowDataParsingError struct {
	Field string
	Msg   string
}

func (err WorkflowDataParsingError) Error() string {
	return fmt.Sprintf("Error when parsing workflow data, error on field %s, %s", err.Field, err.Msg)
}
