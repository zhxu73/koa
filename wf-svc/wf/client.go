package wf

import (
	"gitlab.com/zhxu73/koa/wf-svc/config"
)

// WorkflowClient is the interface for using all workflows
type WorkflowClient interface {
	Configure(*config.Config) error
	DecodeData(dat map[string]interface{}) error
	PerformAction(action string) error
	Actions() []string
}

// CreateWorkflowClient creates a WorkflowClient based on type and provider
func CreateWorkflowClient(provider config.Provider, wfType string) (WorkflowClient, error) {

	switch wfType {
	case "instance_deploy":
		return CreateDeployClient(provider, config.GlobalConfig()), nil
	case "imaging":
		return CreateImagingClient(provider, config.GlobalConfig()), nil
	}
	return nil, UnknownWorkflow{wfType}
}
