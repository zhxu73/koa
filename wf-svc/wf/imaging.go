package wf

import (
	"github.com/google/uuid"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/zhxu73/koa/wf-svc/config"
)

// ImagingClient is responsible for creating image from instance
type ImagingClient struct {
	provider config.Provider
	conf     *config.Config
	param    ImagingParam
}

type ImagingParam struct {
	instance  uuid.UUID `mapstructure:"instance_uuid"`
	username  string    `mapstructure:"username"`
	imageName string    `mapstructure:"image_name"`
}

func CreateImagingClient(provider config.Provider, conf *config.Config) ImagingClient {
	return ImagingClient{provider: provider, conf: conf}
}

func (cli ImagingClient) Configure(conf *config.Config) error {
	cli.conf = conf
	return nil
}

func (cli ImagingClient) DecodeData(dat map[string]interface{}) error {
	return mapstructure.Decode(dat, &cli.param)
}

func (cli ImagingClient) PerformAction(action string) error {
	switch action {
	case "create":
		_, err := cli.createImage()
		if err != nil {
			return err
		}
	default:
		return UnknownWorkflowAction{}
	}
	return nil
}

func (cli ImagingClient) Actions() []string {
	return []string{"create"}
}

func (cli ImagingClient) createImage() (string, error) {
	return "", nil
}
