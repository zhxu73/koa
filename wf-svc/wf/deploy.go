package wf

import (
	"io/ioutil"
	"log"
	"net"
	"path"

	"gitlab.com/zhxu73/koa/wf-svc/config"

	wfv1 "github.com/argoproj/argo/pkg/apis/workflow/v1alpha1"
	"github.com/argoproj/argo/workflow/common"
	"github.com/google/uuid"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/zhxu73/koa/wf-svc/argowf"
)

// DeployClient implements WorkflowClient
type DeployClient struct {
	provider config.Provider
	conf     *config.Config
	param    *DeployParam
}

func CreateDeployClient(provider config.Provider, conf *config.Config) DeployClient {
	return DeployClient{provider: provider, conf: conf, param: &DeployParam{}}
}

func (cli DeployClient) Configure(conf *config.Config) error {
	cli.conf = conf
	return nil
}

func (cli DeployClient) DecodeData(dat map[string]interface{}) error {
	type DeployParamStaging struct {
		Instance      string `mapstructure:"instance_uuid"`
		ServerIP      string `mapstructure:"server_ip"`
		Username      string `mapstructure:"username"`
		CallbackURL   string `mapstructure:"callback_url"`
		CallbackToken string `mapstructure:"callback_token"`
		Redeploy      bool   `mapstructure:"redeploy"`
	}

	var staging DeployParamStaging
	err := mapstructure.Decode(dat, &staging)

	instanceUUID, err := uuid.Parse(staging.Instance)
	if err != nil {
		return err
	}
	cli.param.instance = instanceUUID

	serverIP := net.ParseIP(staging.ServerIP)
	if serverIP == nil {
		return WorkflowDataParsingError{Field: "server_ip", Msg: "cannot parse as ip addr"}
	}
	cli.param.serverIP = serverIP

	if len(staging.Username) <= 0 {
		return WorkflowDataParsingError{Field: "username", Msg: "username missing"}
	}
	cli.param.username = staging.Username

	if len(staging.CallbackURL) <= 0 {
		return WorkflowDataParsingError{Field: "callback_url", Msg: "callback_url missing"}
	}
	cli.param.callbackURL = staging.CallbackURL

	if len(staging.CallbackToken) <= 0 {
		return WorkflowDataParsingError{Field: "callback_token", Msg: "callback_token missing"}
	}
	cli.param.callbackToken = staging.CallbackToken
	cli.param.redeploy = staging.Redeploy

	return nil
}

func (cli DeployClient) Actions() []string {
	return []string{"create"}
}

func (cli DeployClient) PerformAction(action string) error {
	switch action {
	case "create":
		_, err := cli.deployInstance(cli.param.instance, cli.param.serverIP, cli.param.username)
		if err != nil {
			return err
		}
	default:
		return UnknownWorkflowAction{}
	}
	return nil
}

// DeployInstance returns the name of the workflow and possible error
func (cli DeployClient) deployInstance(instanceUUID uuid.UUID, serverIP net.IP, username string) (string, error) {
	conf := cli.conf.ProviderConfig(cli.provider)
	if conf == nil {
		log.Println("no provider config")
		return "", nil
	}

	wfDef, err := readDeployWorkflow(conf.WorkflowBaseDir, cli.provider)
	if err != nil {
		return "", err
	}
	cli.injectParam(wfDef)
	ctx := config.CreateArgoContextFromConfig(cli.conf.ProviderConfig(cli.provider))
	wf, err := argowf.CreateWorkflow(ctx, wfDef)
	if err != nil {
		return "", err
	}
	log.Printf("created workflow %s\n", wf.Name)
	return wf.Name, nil
}

func (cli DeployClient) injectParam(wfDef *wfv1.Workflow) {
	injectWorkflowParameter(wfDef, "server-ip", cli.param.serverIP.String())
	injectWorkflowParameter(wfDef, "user", cli.param.username)
	injectWorkflowParameter(wfDef, "tz", "")
	injectWorkflowParameter(wfDef, "zoneinfo", "")
}

type DeployParam struct {
	instance      uuid.UUID `mapstructure:"instance_uuid"`
	serverIP      net.IP    `mapstructure:"server_ip"`
	username      string    `mapstructure:"username"`
	callbackURL   string    `mapstructure:"callback_url"`
	callbackToken string    `mapstructure:"callback_token"`
	redeploy      bool      `mapstructure:"redeploy"`
}

func readDeployWorkflow(baseDir string, providerUUID config.Provider) (*wfv1.Workflow, error) {
	wfPath := path.Join(baseDir, providerUUID.String(), "instance_deploy.yml")
	dat, err := ioutil.ReadFile(wfPath)
	if err != nil {
		return nil, err
	}
	wfDefs, err := common.SplitWorkflowYAMLFile(dat, true)
	if err != nil {
		return nil, err
	}
	if len(wfDefs) > 1 {
		log.Fatal("File contains more than 1 workflow")
	} else if len(wfDefs) == 0 {
		log.Fatal("File does not containe workflow")
	}
	return &wfDefs[0], nil
}

func injectWorkflowParameter(wfDef *wfv1.Workflow, name string, value string) *wfv1.Workflow {
	param := findWorkflowParameter(wfDef, name)

	// create if not exist
	if param == nil {
		param = &wfv1.Parameter{}
		param.Name = name
		wfDef.Spec.Arguments.Parameters = append(wfDef.Spec.Arguments.Parameters, *param)
	}
	*param.Value = value

	return wfDef
}

func findWorkflowParameter(wfDef *wfv1.Workflow, name string) *wfv1.Parameter {
	for _, param := range wfDef.Spec.Arguments.Parameters {
		if param.Name == name {
			return &param
		}
	}
	return nil
}
