package main

import (
	"log"

	//"gitlab.com/zhxu73/koa/wf-svc/argowf"
	"gitlab.com/zhxu73/koa/wf-svc/config"
	"gitlab.com/zhxu73/koa/wf-svc/nats"
)

func main() {

	err := config.LoadGlobalConfigFromFile("config.yml")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Config loaded")
	//argowf.CreateHelloWorldWorkflow(config.CreateArgoContextFromConfig(config.GlobalConfig().DefaultConfig()))

	nats.Start(config.GlobalConfig(), "test-cluster", "wf-service-1")
}
