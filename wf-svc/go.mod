module gitlab.com/zhxu73/koa/wf-svc

go 1.14

require (
	github.com/nats-io/stan.go v0.7.0
	github.com/argoproj/argo v0.0.0-20200806220847-5759a0e198d3 // v2.9.5
	github.com/argoproj/pkg v0.0.0-20200424003221-9b858eff18a1
	github.com/google/uuid v1.1.1
	github.com/mitchellh/mapstructure v1.3.3
	gopkg.in/yaml.v2 v2.2.8
	k8s.io/api v0.0.0-20191219150132-17cfeff5d095
	k8s.io/apimachinery v0.16.7-beta.0
	k8s.io/client-go v0.0.0-20191225075139-73fd2ddc9180
)
