package nats

import (
	"log"

	"github.com/nats-io/stan.go"
)

// Msg is the Encoded NATS msg data that this service will accept
type Msg interface {
	Encode() ([]byte, error)
}

// MsgDecoder decodes data in NATS msg into Msg
type MsgDecoder interface {
	Decode([]byte) (Msg, error)
}

type MsgHandler interface {
	Decoder() MsgDecoder
	Handle(Msg) error
}

func consumeNATSMsg(m *stan.Msg) {

	var handler MsgHandler
	handler = WorkflowMsgHandler{}

	msg, err := handler.Decoder().Decode(m.Data)
	if err != nil {
		log.Println(err)
		return
	}
	err = handler.Handle(msg)
	if err != nil {
		log.Println(err)
		return
	}
}
