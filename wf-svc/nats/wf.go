package nats

import (
	"encoding/json"
	"log"

	"gitlab.com/zhxu73/koa/wf-svc/wf"

	"github.com/google/uuid"
)

// WorkflowMsg is the msg that the workflow service accepts
type WorkflowMsg struct {
	Namespace string    `json:"namespace"`
	Provider  uuid.UUID `json:"provider"`
	// action to be performed on the workflow
	Action       string `json:"action"`
	WorkflowType string `json:"workflow_type"`
	// WorkflowData data to be passed to workflow
	WorkflowData map[string]interface{} `json:"workflow_data"`
}

func (m WorkflowMsg) Encode() ([]byte, error) {
	dat, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	return dat, nil
}

// WorkflowMsgHandler is handler for WorkflowMsg
type WorkflowMsgHandler struct {
}

// Decoder returns the decoder for WorkflowMsg
func (h WorkflowMsgHandler) Decoder() MsgDecoder {
	return WorkflowMsgDecoder{}
}

// Handle handles WorkflowMsg
func (h WorkflowMsgHandler) Handle(m Msg) error {

	if m == nil {
		log.Println("Msg is nil")
		return BadMsgType{}
	}
	msg, ok := m.(WorkflowMsg)
	if !ok {
		log.Println("Wrong type being dispatched here")
		return BadMsgType{}
	}
	h.typeDispatch(msg.WorkflowType, &msg)
	return nil
}

// dispatch based on workflow type
func (h WorkflowMsgHandler) typeDispatch(wfType string, msg *WorkflowMsg) {
	cli, err := wf.CreateWorkflowClient(msg.Provider, wfType)
	if err != nil {
		log.Println(err)
		return
	}
	err = cli.DecodeData(msg.WorkflowData)
	if err != nil {
		log.Println(err)
		return
	}
	err = cli.PerformAction(msg.Action)
	if err != nil {
		log.Println(err)
	}
}

// WorkflowMsgDecoder decode raw bytes into WorkflowData
type WorkflowMsgDecoder struct {
}

// Decode decodes bytes into WorkflowMsg
func (d WorkflowMsgDecoder) Decode(dat []byte) (Msg, error) {

	var msg WorkflowMsg
	err := json.Unmarshal(dat, &msg)
	if err != nil {
		return nil, err
	}
	return msg, err
}
