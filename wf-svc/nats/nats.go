package nats

import (
	"log"
	"runtime"

	"github.com/nats-io/stan.go"
	"gitlab.com/zhxu73/koa/wf-svc/config"
)

// Start will start the argo workflow service, and subscribe to NATS streaming
// Note: this effectivily puts main thread to sleep
func Start(config *config.Config, clusterID string, clientID string) {

	sc, err := stan.Connect(clusterID, clientID)
	if err != nil {
		log.Fatalln(err)
	}

	// Simple Async Subscriber
	sub, err := sc.Subscribe("workflow", consumeNATSMsg, stan.DurableName("wf-service"))
	if err != nil {
		log.Fatalln(err)
	}
	defer end(sc, sub)

	for {
		runtime.Gosched()
	}
}

func end(sc stan.Conn, sub stan.Subscription) {
	// Unsubscribe
	err := sub.Unsubscribe()
	if err != nil {
		log.Fatalln(err)
	}

	// Close connection
	err = sc.Close()
	if err != nil {
		log.Fatalln(err)
	}
}
