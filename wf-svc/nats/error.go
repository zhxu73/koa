package nats

type BadMsgType struct {
}

func (err BadMsgType) Error() string {
	return "Bad msg type"
}
