package argowf

import (
	"context"
	"log"

	"github.com/argoproj/argo/pkg/apiclient"
	"github.com/argoproj/argo/pkg/apiclient/workflow"
	wfv1 "github.com/argoproj/argo/pkg/apis/workflow/v1alpha1"
)

// ArgoContext is the context of which workflows exists in
type ArgoContext struct {
	ServerURL string
	Secure    bool
	SSLVerify bool
	Namespace string
	Token     string
}

// getClient returns an API client to the gRPC API, and the context to use it with
func getClient(argoCtx *ArgoContext) (context.Context, apiclient.Client) {

	opts := apiclient.Opts{
		ArgoServerOpts: apiclient.ArgoServerOpts{
			URL:                argoCtx.ServerURL,
			Secure:             argoCtx.Secure,
			InsecureSkipVerify: !argoCtx.SSLVerify,
		},
		AuthSupplier: func() string { return "Bearer " + argoCtx.Token },
	}
	ctx, cli, err := apiclient.NewClientFromOpts(opts)
	if err != nil {
		log.Fatal(err)
	}
	return ctx, cli
}

// CreateWorkflow creates a workflow with given context and workflow definition
func CreateWorkflow(argoCtx *ArgoContext, wfDef *wfv1.Workflow) (*wfv1.Workflow, error) {
	var req workflow.WorkflowCreateRequest
	ctx, cli := getClient(argoCtx)

	req.Namespace = argoCtx.Namespace
	req.InstanceID = ""
	req.ServerDryRun = false
	req.Workflow = wfDef

	wf, err := cli.NewWorkflowServiceClient().CreateWorkflow(ctx, &req)
	if err != nil {
		return nil, err
	}
	return wf, nil
}

// WorkflowStatus fetches status of a workflow
func WorkflowStatus(argoCtx *ArgoContext, wfName string) (*wfv1.WorkflowStatus, error) {
	var req workflow.WorkflowGetRequest
	ctx, cli := getClient(argoCtx)

	req.Namespace = argoCtx.Namespace
	req.Name = wfName
	req.Fields = "status.phase"

	wf, err := cli.NewWorkflowServiceClient().GetWorkflow(ctx, &req)
	if err != nil {
		return nil, err
	}
	return &wf.Status, nil
}

// DeleteWorkflow deletes a workflow
func DeleteWorkflow(argoCtx *ArgoContext, wfName string) error {
	var req workflow.WorkflowDeleteRequest
	ctx, cli := getClient(argoCtx)

	req.Namespace = argoCtx.Namespace
	req.Name = wfName

	_, err := cli.NewWorkflowServiceClient().DeleteWorkflow(ctx, &req)
	if err != nil {
		return err
	}
	return nil
}

// ResubmitWorkflow resubmit a workflow
func ResubmitWorkflow(argoCtx *ArgoContext, wfName string) (*wfv1.Workflow, error) {
	var req workflow.WorkflowResubmitRequest
	ctx, cli := getClient(argoCtx)

	req.Namespace = argoCtx.Namespace
	req.Name = wfName

	wf, err := cli.NewWorkflowServiceClient().ResubmitWorkflow(ctx, &req)
	if err != nil {
		return wf, err
	}
	return wf, nil
}

// WorkflowPodLogs fetches logs for a pod in the workflow
func WorkflowPodLogs(argoCtx *ArgoContext, wfName string, podName string) ([]string, error) {
	var req workflow.WorkflowLogRequest
	ctx, cli := getClient(argoCtx)

	req.Namespace = argoCtx.Namespace
	req.Name = wfName
	req.PodName = podName
	req.LogOptions.Timestamps = true

	logCli, err := cli.NewWorkflowServiceClient().PodLogs(ctx, &req)
	if err != nil {
		return nil, err
	}
	entries := make([]string, 1)
	for err == nil {
		var entry *workflow.LogEntry
		entry, err = logCli.Recv()
		if entry != nil {
			entries = append(entries, entry.Content)
		}
	}
	if err != nil {
		return nil, err
	}
	return entries, nil
}
