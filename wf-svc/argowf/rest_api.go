package argowf

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	wfv1 "github.com/argoproj/argo/pkg/apis/workflow/v1alpha1"
)

type workflowCreateRESTPayload struct {
	Namespace    string        `json:"namespace"`
	ServerDryRun bool          `json:"serverDryRun"`
	Workflow     wfv1.Workflow `json:"workflow"`
}

// SubmitWorkflowREST submit workflow via REST API
func SubmitWorkflowREST(hostnamePort string, namespace string, token string, wf wfv1.Workflow) ([]byte, error) {

	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	client := http.Client{Timeout: time.Duration(30 * time.Second)}

	// construct request body
	reqPayload := workflowCreateRESTPayload{Namespace: namespace, ServerDryRun: true, Workflow: wf}

	// serialize payload
	wfBytes, err := json.Marshal(reqPayload)
	if err != nil {
		return nil, err
	}

	// prep request
	url := fmt.Sprintf("https://%s/api/v1/workflows/%s", hostnamePort, namespace)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(wfBytes))
	req.Header.Set("Content-type", "application/json")
	req.Header.Set("Authorization", "Bearer "+token)
	if err != nil {
		return nil, err
	}

	// send request
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// read response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}
