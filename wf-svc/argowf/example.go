package argowf

import (
	"log"

	wfv1 "github.com/argoproj/argo/pkg/apis/workflow/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var HelloWorldWorkflow = wfv1.Workflow{
	TypeMeta: metav1.TypeMeta{APIVersion: "argoproj.io/v1alpha1", Kind: "Workflow"},
	ObjectMeta: metav1.ObjectMeta{
		GenerateName: "hello-world-",
	},
	Spec: wfv1.WorkflowSpec{
		ServiceAccountName: "atmosphere",
		Entrypoint:         "whalesay",
		Templates: []wfv1.Template{
			{
				Name: "whalesay",
				Container: &corev1.Container{
					Image:   "docker/whalesay:latest",
					Command: []string{"cowsay"},
					Args:    []string{"hello world"},
				},
			},
		},
	},
}

func CreateHelloWorldWorkflow(argoCtx *ArgoContext) {
	wf, err := CreateWorkflow(argoCtx, &HelloWorldWorkflow)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(wf.Name)
}
