# Workflow Service

Workflow service is event driven. It subscribe to NATS messages, and perform workflow actions specified in the msg.


## NATS Message

The NATS message that this service accepts is JSON encoded.

e.g.
> {
>   "provider": "1234-1234-1234-1234",
>   "namespace": "argo",
>   "workflow_type": "instance_deploy",
>   "action": "create",
>   "workflow_data": {},
> }

## Workflows

- instance_deploy
- imaging


