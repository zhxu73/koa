package base

import (
	"net/http"

	log "github.com/sirupsen/logrus"
)

func DeployVersion(w http.ResponseWriter, r *http.Request) {
	log.Info("deploy_version triggered")
}
