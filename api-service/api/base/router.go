package base

import "github.com/gorilla/mux"

func SetupBaseRouter(router *mux.Router) {
	router.HandleFunc("/version", Version)
	router.HandleFunc("/deploy_version", DeployVersion)
}
