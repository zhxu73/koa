package v2

import (
	log "github.com/sirupsen/logrus"
	"net/http"
)

func Images(w http.ResponseWriter, r *http.Request) {
	log.Info("images triggered")
}

func ImageAccessLists(w http.ResponseWriter, r *http.Request) {
	log.Info("image_access_lists triggered")
}

func ImageBookmarks(w http.ResponseWriter, r *http.Request) {
	log.Info("image_bookmarks triggered")
}

func ImageMetrics(w http.ResponseWriter, r *http.Request) {
	log.Info("image_metrics triggered")
}

func ImageTags(w http.ResponseWriter, r *http.Request) {
	log.Info("image_tags triggered")
}

func ImageVersions(w http.ResponseWriter, r *http.Request) {
	log.Info("image_versions triggered")
}

func ImageVersionLicenses(w http.ResponseWriter, r *http.Request) {
	log.Info("image_version_licenses triggered")
}

func ImageVersionMemberships(w http.ResponseWriter, r *http.Request) {
	log.Info("image_version_memberships triggered")
}

func ImageVersionBootScripts(w http.ResponseWriter, r *http.Request) {
	log.Info("image_version_boot_scripts triggered")
}
