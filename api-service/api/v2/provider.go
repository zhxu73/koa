package v2

import (
	log "github.com/sirupsen/logrus"
	"net/http"
)

func Providers(w http.ResponseWriter, r *http.Request) {
	log.Info("providers triggered")
}

func ProviderMachines(w http.ResponseWriter, r *http.Request) {
	log.Info("provider_machines triggered")
}

func ProviderTypes(w http.ResponseWriter, r *http.Request) {
	log.Info("provider_types triggered")
}
