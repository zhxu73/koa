package v2

import (
	log "github.com/sirupsen/logrus"
	"net/http"
)

func Users(w http.ResponseWriter, r *http.Request) {
	log.Info("users triggered")
}

func UserAllocationSources(w http.ResponseWriter, r *http.Request) {
	log.Info("user_allocation_sources triggered")
}
