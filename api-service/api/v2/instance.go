package v2

import (
	"net/http"

	log "github.com/sirupsen/logrus"
)

func Instances(w http.ResponseWriter, r *http.Request) {
	log.Info("instances triggered")
}

func InstanceActions(w http.ResponseWriter, r *http.Request) {
	log.Info("instance_actions triggered")
}

func InstanceAllocationSource(w http.ResponseWriter, r *http.Request) {
	log.Info("instance_allocation_source triggered")
}

func InstanceHistories(w http.ResponseWriter, r *http.Request) {
	log.Info("instance_histories triggered")
}

func InstanceTags(w http.ResponseWriter, r *http.Request) {
	log.Info("instance_tags triggered")
}
