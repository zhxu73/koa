package v2

import (
	"net/http"

	log "github.com/sirupsen/logrus"
)

func EmulateToken(w http.ResponseWriter, r *http.Request) {
	log.Info("emulate_token triggered")
}

func EmulateSession(w http.ResponseWriter, r *http.Request) {
	log.Info("emulate_session triggered")
}
