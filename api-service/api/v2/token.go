package v2

import (
	log "github.com/sirupsen/logrus"
	"net/http"
)

func TokenUpdate(w http.ResponseWriter, r *http.Request) {
	log.Info("token_update triggered")
}

func Tokens(w http.ResponseWriter, r *http.Request) {
	log.Info("tokens triggered")
}
