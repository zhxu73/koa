package v2

import (
	log "github.com/sirupsen/logrus"
	"net/http"
)

func Projects(w http.ResponseWriter, r *http.Request) {
	log.Info("projects triggered")
}

func ProjectLinks(w http.ResponseWriter, r *http.Request) {
	log.Info("project_links triggered")
}

func ProjectImages(w http.ResponseWriter, r *http.Request) {
	log.Info("project_images triggered")
}

func ProjectInstances(w http.ResponseWriter, r *http.Request) {
	log.Info("project_instances triggered")
}

func ProjectVolumes(w http.ResponseWriter, r *http.Request) {
	log.Info("project_volumes triggered")
}
