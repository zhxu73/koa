package v2

import (
	log "github.com/sirupsen/logrus"
	"net/http"
)

func MaintenanceRecords(w http.ResponseWriter, r *http.Request) {
	log.Info("maintenance_records triggered")
}
