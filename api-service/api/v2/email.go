package v2

import (
	log "github.com/sirupsen/logrus"
	"net/http"
)

func EmailFeedback(w http.ResponseWriter, r *http.Request) {
	log.Infoln("email_feedback triggered")
}

func EmailInstanceReport(w http.ResponseWriter, r *http.Request) {
	log.Infoln("email_instance_report triggered")
}

func EmailRequestResources(w http.ResponseWriter, r *http.Request) {
	log.Infoln("email_request_resources triggered")
}

func EmailTemplate(w http.ResponseWriter, r *http.Request) {
	log.Infoln("email_template triggered")
}

func EmailVolumeReport(w http.ResponseWriter, r *http.Request) {
	log.Infoln("email_volume_report triggered")
}
