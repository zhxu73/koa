package main

import (
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"gitlab.com/cyverse/koa/api-service/api"
	"gitlab.com/cyverse/koa/api-service/api/base"
	v2 "gitlab.com/cyverse/koa/api-service/api/v2"
)

const (
	defaultKeycloakURL         = "http://172.20.0.1:8080/auth/realms/master"
	defaultKeycloakRedirectURL = "http://localhost:8000/user/login/callback"
	defaultKeycloakClientID    = "koa"
)

func init() {
	// Configure logger
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	logger := log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	})

	err = api.InitKeycloakClient(
		defaultKeycloakURL,
		defaultKeycloakRedirectURL,
		defaultKeycloakClientID,
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Panic("unable to connect to Keycloak")
		panic(err)
	}
	logger.Info("initialized connection to Keycloak")

}

func main() {
	router := setupRouter()

	log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	}).Info("API Service listening on port 8000")
	http.ListenAndServe(":8000", router)
}

func setupRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	api.AuthAPIRouter(router)

	subRouter := router.NewRoute().Subrouter()
	subRouter.Use(api.AuthenticationMiddleware)

	// Setup routers for v1 & v2 endpoints
	base.SetupBaseRouter(subRouter)
	// v1.SetupV1Router(subRouter)
	v2.SetupV2Router(subRouter)

	return router
}
